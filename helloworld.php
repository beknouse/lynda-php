<!DOCTYPE html PUBLIC "-//W3C//DDT HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
    <head>
        <title>untitled</title>
    </head>

    <body>
        <?php
        // single line comment
        # or another single line comment
        /* multi line comments look
        like this
        crazy huh? */ ?>
        <?php echo "Hello World"; ?> <br />
        <?php echo "Hello" . " World"; ?>
        <?php echo 2+3; ?>
    </body>
</html>